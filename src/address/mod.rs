// src/address/mod.rs
//
// Copyright (c) 2021
// Jeff Nettleton
// jeffdn@gmail.com

mod model;
mod routes;

pub use model::*;
pub use routes::init;

