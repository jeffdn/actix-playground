// src/address/model.rs
//
// Copyright (c) 2021
// Jeff Nettleton
// jeffdn@gmail.com

use anyhow::Result;
use serde::{Serialize, Deserialize};
use sqlx::{FromRow, PgPool};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, FromRow)]
pub struct Address {
    #[serde(default)]
    id:             Uuid,
    person_id:      Uuid,
    street_address: String,
    city:           String,
    state:          String,
    zip_code:       String,
    move_in:        chrono::NaiveDate,
    move_out:       Option<chrono::NaiveDate>,
    is_active:      bool,
}

#[derive(Debug, Deserialize)]
pub struct AddressQuery {
    person_id:      Option<Uuid>,
    city:           Option<String>,
    state:          Option<String>,
    zip_code:       Option<String>,
    resided_on:     Option<chrono::NaiveDate>,
    is_active:      Option<bool>,
}

impl Address {
    pub async fn get_all(pool: &PgPool) -> Result<Vec<Address>> {
        let rows = sqlx::query_as!(Address, r#"
            select id,
                   person_id,
                   street_address,
                   city,
                   state,
                   zip_code,
                   move_in,
                   move_out,
                   is_active
            from address
        "#).fetch_all(pool).await?;

        Ok(rows.into_iter().collect())
    }

    pub async fn get(id: Uuid, pool: &PgPool) -> Result<Address> {
        let address = sqlx::query_as!(Address, r#"
            select id,
                   person_id,
                   street_address,
                   city,
                   state,
                   zip_code,
                   move_in,
                   move_out,
                   is_active
            from address
            where id = $1
        "#,
            id,
        )
            .fetch_one(pool)
            .await?;

        Ok(address)
    }

    pub async fn find(query: AddressQuery, pool: &PgPool) -> Result<Vec<Address>> {
        let mut bind_strs: Vec<String> = vec!();
        let mut base_search: String = r#"
            select id,
                   person_id,
                   street_address,
                   city,
                   state,
                   zip_code,
                   move_in,
                   move_out,
                   is_active
            from address
            where
        "#.into();

        if let (None, None, None, None, None, None) = (
            &query.person_id, &query.city, &query.state,
            &query.zip_code, &query.resided_on, &query.is_active
        ) {
            return Address::get_all(pool).await;
        }

        if query.person_id.is_some() {
            bind_strs.push(format!("person_id = ${}", bind_strs.len() + 1));
        }

        if query.city.is_some() {
            bind_strs.push(format!("city ilike ${}", bind_strs.len() + 1));
        }

        if query.state.is_some() {
            bind_strs.push(format!("state = ${}", bind_strs.len() + 1));
        }

        if query.zip_code.is_some() {
            bind_strs.push(format!("zip_code = ${}", bind_strs.len() + 1));
        }

        if query.resided_on.is_some() {
            bind_strs.push(format!(r#"(
               ${} between move_in and move_out
            or (move_in < ${} and move_out is null)
            )"#,
                bind_strs.len() + 1, bind_strs.len() + 2)
            );
        }

        if query.is_active.is_some() {
            bind_strs.push(format!("is_active = ${}", bind_strs.len() + 1));
        }

        base_search.push_str(&bind_strs.join(" and \n"));
        let mut query_obj = sqlx::query_as(&base_search);

        if let Some(ref person_id) = query.person_id {
            query_obj = query_obj.bind(person_id);
        }

        if let Some(ref city) = query.city {
            query_obj = query_obj.bind(format!("%{}%", city));
        }

        if let Some(ref state) = query.state {
            query_obj = query_obj.bind(state);
        }

        if let Some(ref zip_code) = query.zip_code {
            query_obj = query_obj.bind(zip_code);
        }

        if let Some(ref resided_on) = query.resided_on {
            query_obj = query_obj.bind(resided_on);
            query_obj = query_obj.bind(resided_on);
        }

        if let Some(ref is_active) = query.is_active {
            query_obj = query_obj.bind(is_active);
        }

        let addresses: Vec<Address> = query_obj.fetch_all(pool).await?;

        Ok(addresses)
    }

    pub async fn create(address: Address, pool: &PgPool) -> Result<Address> {
        let mut tx = pool.begin().await.unwrap();
        let address = sqlx::query_as!(Address, r#"
            insert into address (
              person_id,
              street_address,
              city,
              state,
              zip_code,
              move_in,
              move_out,
              is_active
            )
            values ($1, $2, $3, $4, $5, $6, $7, $8)
            returning id,
                      person_id,
                      street_address,
                      city,
                      state,
                      zip_code,
                      move_in,
                      move_out,
                      is_active
        "#,
            address.person_id,
            address.street_address,
            address.city,
            address.state,
            address.zip_code,
            address.move_in,
            address.move_out,
            address.is_active
        )
            .fetch_one(&mut tx)
            .await?;

        tx.commit().await.unwrap();

        Ok(address)
    }

    pub async fn update(id: Uuid, address: Address, pool: &PgPool) -> Result<Address> {
        let mut tx = pool.begin().await.unwrap();
        let address = sqlx::query_as!(Address, r#"
            update address
            set person_id = $1,
                street_address = $2,
                city = $3,
                state = $4,
                zip_code = $5,
                move_in = $6,
                move_out = $7,
                is_active = $8
            where id = $9
            returning id,
                      person_id,
                      street_address,
                      city,
                      state,
                      zip_code,
                      move_in,
                      move_out,
                      is_active
        "#,
            address.person_id,
            address.street_address,
            address.city,
            address.state,
            address.zip_code,
            address.move_in,
            address.move_out,
            address.is_active,
            id,
        )
            .fetch_one(&mut tx)
            .await?;

        tx.commit().await.unwrap();

        Ok(address)
    }

    pub async fn delete(id: Uuid, pool: &PgPool) -> Result<Address> {
        let mut tx = pool.begin().await.unwrap();
        let address = sqlx::query_as!(Address, r#"
            delete
            from address
            where id = $1
            returning id,
                      person_id,
                      street_address,
                      city,
                      state,
                      zip_code,
                      move_in,
                      move_out,
                      is_active
        "#,
            id,
        )
            .fetch_one(&mut tx)
            .await?;

        tx.commit().await.unwrap();

        Ok(address)
    }
}

impl AddressQuery {
    pub fn for_person(person_id: Uuid) -> Self {
        AddressQuery {
            person_id: Some(person_id),
            city: None,
            state: None,
            zip_code: None,
            resided_on: None,
            is_active: None,
        }
    }
}
