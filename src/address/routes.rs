// src/address/routes.rs
//
// Copyright (c) 2021
// Jeff Nettleton
// jeffdn@gmail.com

use actix_web::{delete, get, post, put, web, HttpResponse, Responder};
use sqlx::PgPool;
use uuid::Uuid;

use crate::address::{Address, AddressQuery};

#[get("/address")]
async fn get_all(db_pool: web::Data<PgPool>) -> impl Responder {
    let result = Address::get_all(db_pool.get_ref()).await;

    match result {
        Ok(addresses) => HttpResponse::Ok().json(addresses),
        _ => HttpResponse::BadRequest().body("Error trying to fetch all address records from database"),
    }
}

#[get("/address/search")]
async fn find(query: web::Query<AddressQuery>, db_pool: web::Data<PgPool>) -> impl Responder {
    let result = Address::find(query.into_inner(), db_pool.get_ref()).await;

    match result {
        Ok(address) => HttpResponse::Ok().json(address),
        Err(e) => HttpResponse::BadRequest().body(format!("Error trying to handle the provided URL query {:?}", e)),
    }
}

#[get("/address/{id}")]
async fn get(id: web::Path<Uuid>, db_pool: web::Data<PgPool>) -> impl Responder {
    let get_id: Uuid = id.into_inner();
    let result = Address::get(get_id, db_pool.get_ref()).await;

    match result {
        Ok(address) => HttpResponse::Ok().json(address),
        _ => HttpResponse::BadRequest().body(format!("Error trying to fetch the address with id {}", get_id)),
    }
}

#[post("/address")]
async fn create(address: web::Json<Address>, db_pool: web::Data<PgPool>) -> impl Responder {
    let result = Address::create(address.into_inner(), db_pool.get_ref()).await;

    match result {
        Ok(address) => HttpResponse::Ok().json(address),
        _ => HttpResponse::BadRequest().body("Unable to create the specified address record"),
    }
}

#[put("/address/{id}")]
async fn update(id: web::Path<Uuid>, address: web::Json<Address>, db_pool: web::Data<PgPool>) -> impl Responder {
    let for_id = id.into_inner();
    let result = Address::update(for_id, address.into_inner(), db_pool.get_ref()).await;

    match result {
        Ok(address) => HttpResponse::Ok().json(address),
        _ => HttpResponse::BadRequest().body(format!("Error tryping to update the address with id {}", for_id)),
    }
}

#[delete("/address/{id}")]
async fn delete(id: web::Path<Uuid>, db_pool: web::Data<PgPool>) -> impl Responder {
    let for_id: Uuid = id.into_inner();
    let result = Address::delete(for_id, db_pool.get_ref()).await;

    match result {
        Ok(address) => HttpResponse::Ok().json(address),
        _ => HttpResponse::BadRequest().body(format!("Error trying to delete the address with id {}", for_id)),
    }
}

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(get_all);
    cfg.service(find);
    cfg.service(get);
    cfg.service(create);
    cfg.service(update);
    cfg.service(delete);
}

