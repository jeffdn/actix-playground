// src/person/model.rs
//
// Copyright (c) 2021
// Jeff Nettleton
// jeffdn@gmail.com

use anyhow::Result;
use serde::{Serialize, Deserialize};
use sqlx::{FromRow, PgPool};
use uuid::Uuid;

use crate::address::{Address, AddressQuery};

#[derive(Debug, Serialize, Deserialize, FromRow)]
pub struct Person {
    #[serde(default)]
    id:         Uuid,
    first_name: String,
    last_name:  String,
    dob:        chrono::NaiveDate,
    #[serde(default = "bool::default")]
    is_cat:     bool,
}

#[derive(Debug, Deserialize)]
pub struct PersonQuery {
    first_name: Option<String>,
    last_name:  Option<String>,
    dob:        Option<chrono::NaiveDate>,
    is_cat:     Option<bool>,
}

#[derive(Debug, Serialize)]
pub struct PersonRecord {
    person: Person,
    addresses: Vec<Address>,
}

impl Person {
    pub async fn get_all(pool: &PgPool) -> Result<Vec<Person>> {
        let rows = sqlx::query_as!(Person, r#"
            select id,
                   first_name,
                   last_name,
                   dob,
                   is_cat
            from person
        "#).fetch_all(pool).await?;

        Ok(rows.into_iter().collect())
    }

    pub async fn get(id: Uuid, pool: &PgPool) -> Result<Person> {
        let person = sqlx::query_as!(Person, r#"
            select id,
                   first_name,
                   last_name,
                   dob,
                   is_cat
            from person
            where id = $1
        "#,
            id,
        )
            .fetch_one(pool)
            .await?;

        Ok(person)
    }

    pub async fn find(query: PersonQuery, pool: &PgPool) -> Result<Vec<Person>> {
        let mut bind_strs: Vec<String> = vec!();
        let mut base_search: String = r#"
            select id,
                   first_name,
                   last_name,
                   dob,
                   is_cat
            from person
            where
        "#.into();

        if let PersonQuery {
            first_name: None,
            last_name: None,
            dob: None,
            is_cat: None
        } = &query {
            return Person::get_all(pool).await;
        }

        if query.first_name.is_some() {
            bind_strs.push(format!("first_name ilike ${}", bind_strs.len() + 1));
        }

        if query.last_name.is_some() {
            bind_strs.push(format!("last_name ilike ${}", bind_strs.len() + 1));
        }

        if query.dob.is_some() {
            bind_strs.push(format!("dob = ${}", bind_strs.len() + 1));
        }

        if query.is_cat.is_some() {
            bind_strs.push(format!("is_cat = ${}", bind_strs.len() + 1));
        }

        base_search.push_str(&bind_strs.join(" or \n"));
        let mut query_obj = sqlx::query_as(&base_search);

        if let Some(ref first) = query.first_name {
            query_obj = query_obj.bind(format!("%{}%", first));
        }

        if let Some(ref last) = query.last_name {
            query_obj = query_obj.bind(format!("%{}%", last));
        }

        if let Some(ref dob) = query.dob {
            query_obj = query_obj.bind(dob);
        }

        if let Some(ref is_cat) = query.is_cat {
            query_obj = query_obj.bind(is_cat);
        }

        let people: Vec<Person> = query_obj.fetch_all(pool).await?;

        Ok(people)
    }

    pub async fn create(person: Person, pool: &PgPool) -> Result<Person> {
        let mut tx = pool.begin().await.unwrap();
        let person = sqlx::query_as!(Person, r#"
            insert into person (first_name, last_name, dob, is_cat)
            values ($1, $2, $3, $4)
            returning id,
                      first_name,
                      last_name,
                      dob,
                      is_cat
        "#,
            person.first_name,
            person.last_name,
            person.dob,
            person.is_cat,
        )
            .fetch_one(&mut tx)
            .await?;

        tx.commit().await.unwrap();

        Ok(person)
    }

    pub async fn update(id: Uuid, person: Person, pool: &PgPool) -> Result<Person> {
        let mut tx = pool.begin().await.unwrap();
        let person = sqlx::query_as!(Person, r#"
            update person
            set first_name = $1,
                last_name = $2,
                dob = $3,
                is_cat = $4
            where id = $5
            returning id,
                      first_name,
                      last_name,
                      dob,
                      is_cat
        "#,
            person.first_name,
            person.last_name,
            person.dob,
            person.is_cat,
            id,
        )
            .fetch_one(&mut tx)
            .await?;

        tx.commit().await.unwrap();

        Ok(person)
    }

    pub async fn delete(id: Uuid, pool: &PgPool) -> Result<Person> {
        let mut tx = pool.begin().await.unwrap();
        let person = sqlx::query_as!(Person, r#"
            delete
            from person
            where id = $1
            returning id,
                      first_name,
                      last_name,
                      dob,
                      is_cat
        "#,
            id,
        )
            .fetch_one(&mut tx)
            .await?;

        tx.commit().await.unwrap();

        Ok(person)
    }
}

impl PersonRecord {
    pub async fn get(id: Uuid, pool: &PgPool) -> Result<PersonRecord> {
        let person = Person::get(id.clone(), pool).await?;
        let addresses = Address::find(AddressQuery::for_person(id.clone()), pool).await?;

        Ok(
            PersonRecord {
                person,
                addresses,
            }
        )
    }
}
