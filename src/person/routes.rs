// src/person/routes.rs
//
// Copyright (c) 2021
// Jeff Nettleton
// jeffdn@gmail.com

use actix_web::{delete, get, post, put, web, HttpResponse, Responder};
use sqlx::PgPool;
use uuid::Uuid;

use crate::person::{Person, PersonQuery, PersonRecord};

#[get("/person")]
async fn get_all(db_pool: web::Data<PgPool>) -> impl Responder {
    let result = Person::get_all(db_pool.get_ref()).await;

    match result {
        Ok(people) => HttpResponse::Ok().json(people),
        _ => HttpResponse::BadRequest().body("Error trying to fetch all person records from database"),
    }
}

#[get("/person/search")]
async fn find(query: web::Query<PersonQuery>, db_pool: web::Data<PgPool>) -> impl Responder {
    let result = Person::find(query.into_inner(), db_pool.get_ref()).await;

    match result {
        Ok(person) => HttpResponse::Ok().json(person),
        Err(e) => HttpResponse::BadRequest().body(format!("Error trying to handle the provided URL query {:?}", e)),
    }
}

#[get("/person/{id}")]
async fn get(id: web::Path<Uuid>, db_pool: web::Data<PgPool>) -> impl Responder {
    let get_id: Uuid = id.into_inner();
    let result = Person::get(get_id, db_pool.get_ref()).await;

    match result {
        Ok(person) => HttpResponse::Ok().json(person),
        _ => HttpResponse::BadRequest().body(format!("Error trying to fetch the person with id {}", get_id)),
    }
}

#[get("/person/{id}/record")]
async fn get_record(id: web::Path<Uuid>, db_pool: web::Data<PgPool>) -> impl Responder {
    let get_id: Uuid = id.into_inner();
    let result = PersonRecord::get(get_id, db_pool.get_ref()).await;

    match result {
        Ok(record) => HttpResponse::Ok().json(record),
        _ => HttpResponse::BadRequest().body(format!("Error trying to fetch the person record for id {}", get_id)),
    }
}

#[post("/person")]
async fn create(person: web::Json<Person>, db_pool: web::Data<PgPool>) -> impl Responder {
    let result = Person::create(person.into_inner(), db_pool.get_ref()).await;

    match result {
        Ok(person) => HttpResponse::Ok().json(person),
        _ => HttpResponse::BadRequest().body("Unable to create the specified person record"),
    }
}

#[put("/person/{id}")]
async fn update(id: web::Path<Uuid>, person: web::Json<Person>, db_pool: web::Data<PgPool>) -> impl Responder {
    let for_id = id.into_inner();
    let result = Person::update(for_id, person.into_inner(), db_pool.get_ref()).await;

    match result {
        Ok(person) => HttpResponse::Ok().json(person),
        _ => HttpResponse::BadRequest().body(format!("Error tryping to update the person with id {}", for_id)),
    }
}

#[delete("/person/{id}")]
async fn delete(id: web::Path<Uuid>, db_pool: web::Data<PgPool>) -> impl Responder {
    let for_id: Uuid = id.into_inner();
    let result = Person::delete(for_id, db_pool.get_ref()).await;

    match result {
        Ok(person) => HttpResponse::Ok().json(person),
        _ => HttpResponse::BadRequest().body(format!("Error trying to delete the person with id {}", for_id)),
    }
}

pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(get_all);
    cfg.service(find);
    cfg.service(get);
    cfg.service(get_record);
    cfg.service(create);
    cfg.service(update);
    cfg.service(delete);
}
