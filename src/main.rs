// src/main.rs
//
// Copyright (c) 2021
// Jeff Nettleton
// jeffdn@gmail.com

mod address;
mod person;

use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use dotenv::dotenv;
use sqlx::PgPool;

async fn index() -> impl Responder {
    HttpResponse::Ok().body("Bugger off, mate!")
}

#[actix_web::main]
async fn main() -> Result<(), std::io::Error> {
    dotenv().ok();
    pretty_env_logger::init();

    let db_pool = web::Data::new(
        PgPool::connect("postgresql://jeff@localhost/jeff").await.unwrap()
    );
    let server = HttpServer::new(move || {
        App::new()
            .app_data(db_pool.clone())
            .route("/", web::get().to(index))
            .configure(address::init)
            .configure(person::init)
    }).bind(("127.0.0.1", 8080))?.run();

    server.await
}
