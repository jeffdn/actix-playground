create extension if not exists "uuid-ossp";

begin;

create table if not exists person (
  id uuid primary key default uuid_generate_v4(),
  first_name text not null,
  last_name text not null,
  dob date not null
);

create table if not exists address (
  id uuid primary key default uuid_generate_v4(),
  person_id uuid not null references person (id),
  street_address text not null,
  city text not null,
  state text not null,
  zip_code text not null,
  move_in date not null,
  move_out date,
  is_active boolean not null default false
);

create unique index if not exists address_person_id_is_active_idx on address (person_id, is_active) where is_active is true;

commit;
